gpr.controller('dashboardController', function($scope,$http,Locations) {
        // create a message to display in our view
        console.log("DASHboard");
        $scope. init = function(){
        	
	        $scope.sampa = {
	        	lat: -23.57,
	        	lon:  -46.60,
	        	zoom: 10
	        }

	        getLocation();
        }

        function getLocation(){
        	Locations.get()
	        	.success(function(getData) {
	                console.log(getData)
	                $scope.markers = [];
	                for(var i=0;i<getData.length;i++){
	                	var item = {};
	                	item.lat = parseFloat(getData[i].lat);
	                	item.lon = parseFloat(getData[i].lon);
	                	item.zoom = parseFloat(getData[i].zoom);
	                	item.label = {};
	                	item.label.message = "GIlg";
					    item.label.show=true;
					    item.label.showOnMouseOver= true;
	                	$scope.markers.push(item);
	                }
	                $scope.users = getData.length;
	            });            
        }

        $scope.submitLocation = function() {
        	$scope.location = {};
        	var first = Math.floor((Math.random() * 9) + 1);
        	var second = Math.floor((Math.random() * 9) + 1);
        	var third = Math.floor((Math.random() * 9) + 1);
        	var fourth = Math.floor((Math.random() * 9) + 1);
        	$scope.location.lat = "-23."+first+second;
        	$scope.location.lon = "-46."+third+fourth;
        	$scope.location.zoom = "1";
	        // save the comment. pass in comment data from the form
	        // use the function we created in our service
	        Locations.save($scope.location)
	            .success(function(data) {

	                getLocation();

	            })
	            .error(function(data) {
	                console.log(data);
	            });
	    };
        
});