var gpr = angular.module('gpr', ["ui.router","openlayers-directive","factoryModel"]);


gpr.config(function($stateProvider, $urlRouterProvider) {
  //
  // For any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/dashboard");
  //
  // Now set up the states
  $stateProvider
    .state('dashboard', {
      url: "/dashboard",
      templateUrl: "grp-app/Modules/Dashboard/dashboard.html",
      controller : "dashboardController"
      
    })
    .state('teste', {
      url: "/teste",
      templateUrl: "grp-app/pages/teste.html"
        
    });
});