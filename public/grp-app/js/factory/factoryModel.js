// public/js/services/commentService.js

angular.module('factoryModel', [])

.factory('Locations', function($http) {

    return {
        // get all the comments
        get : function() {
            return $http.get('/api/locations');
        },

        // save a comment (pass in comment data)
        save : function(locationData) {
            return $http({
                method: 'POST',
                url: '/api/locations',
                headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                data: $.param(locationData)
            });
        },

        // destroy a comment
        destroy : function(id) {
            return $http.delete('/api/locations/' + id);
        }
    }

});