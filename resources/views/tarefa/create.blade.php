<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>Cadastro de Tarefa</title>	  
</head>
<body>
  <form action="{!!URL::route('tarefa.store')!!}" method="post">
    <label for="titulo">Título:</label>
    <input type="text" name="titulo">
    <label for="corpo">Corpo:</label>
    <input type="text" name="corpo">    
    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
    <input type="submit">
  </form>
</body>
</html>	