<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>Lista de Tarefas</title>
</head>
<body>
  <ul>
  <?php foreach ($tarefas as $tarefa): ?>
    <li>
        Id: <?php echo $tarefa->id ?>
        <br/>
        Título: <?php echo $tarefa->titulo ?>
        <br/>
        Corpo: <?php echo $tarefa->corpo ?>
    </li>
  <?php endforeach ?>
  </ul>
  <a href="{!!URL::route('tarefa.create')!!}">Cadastro</a>
</body>
</html>